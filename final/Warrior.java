public class Warrior extends Fighter {
    public Warrior(int baseHp, int wp) {
        super(baseHp, wp);
    }

    @Override
    public double getCombatScore() {
        double pre;
        if (Utility.isPrime((Battle.GROUND))) {
            pre = getBaseHp() * 2;
        } else {
            if (getWp() == 1) {
                pre = getBaseHp();
            } else pre = getBaseHp() / 10;
        }
        if(pre>999){return 999;}
            else return pre;
    }
}
