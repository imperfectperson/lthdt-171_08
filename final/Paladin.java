
public class Paladin extends Knight {
    public Paladin(int baseHp, int wp) {
        super(baseHp, wp);
    }

    @Override
    public double getCombatScore() {
        int n = Utility.isFibo(getBaseHp());
        if (n != -1) return 1000 + n;
        else return getBaseHp() * 3;
    }
}
